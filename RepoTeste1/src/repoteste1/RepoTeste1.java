package repoteste1;

import javax.swing.JOptionPane;

public class RepoTeste1 {
    
    static int a, fatorial, divisao;

    public static void main(String[] args) {
        a = Integer.parseInt(JOptionPane.showInputDialog("Digite um número"));
        fatorial = fat(a);
        divisao = 1/2;
        JOptionPane.showMessageDialog(null, "Fatorial do número é:  " + divisao);
    }

    private static int fat(int x) {
        if (x == 0) {
            return 1;
        } else {
            return x * fat(x - 1);
        }
    }
}

    

